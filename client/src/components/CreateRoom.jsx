import React from 'react';
import { useNavigate } from 'react-router-dom';

const CreateRoom = () => {
    const navigate = useNavigate();

    const create = async (e) => {
        e.preventDefault();

        const resp = await fetch('https://rtc-be.webbythien.com/create');
        const { room_id } = await resp.json();
        console.log('room : ', room_id);
        navigate(`/room/${room_id}`);
    };

    return (
        <div>
            <button onClick={create}>Create Room</button>
        </div>
    );
};

export default CreateRoom;
