import React, { useEffect, useRef } from "react";
import { useParams } from "react-router-dom";

const Room = () => {
  const { roomID } = useParams();
  const userVideo = useRef();
  const userStream = useRef();
  const partnerVideo = useRef();
  const peerRef = useRef();
  const webSocketRef = useRef();
  
  const openCamera = async () => {
  
    try {
      const allDevices = await navigator.mediaDevices.enumerateDevices();
      const cameras = allDevices.filter((device) => device.kind === "videoinput");
      if (cameras.length === 0) {
        console.log("No cameras found.");
        return;
      }
      console.log("Available cameras:", cameras);
  
      const constraints = {
        video: {
          deviceId: cameras[0].deviceId, // Use the first available camera
          width: { min: 640, ideal: 1920, max: 1920 },
          height: { min: 480, ideal: 1080, max: 1080 }
        },
        audio: true
      };
  
      console.log("Get Media");
      console.log("getUserMedia: ", await navigator.mediaDevices.getUserMedia(constraints))

      const stream = await navigator.mediaDevices.getUserMedia(constraints);
      return stream;
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    const startConnection = async () => {
      const stream = await openCamera();
      userVideo.current.srcObject = stream;
      userStream.current = stream;

      console.log("roomID: ", roomID);
      webSocketRef.current = new WebSocket(
        `wss://rtc-be.webbythien.com/join?roomID=${roomID}`
      );

      webSocketRef.current.addEventListener("open", () => {
        webSocketRef.current.send(JSON.stringify({ join: true }));
      });

      webSocketRef.current.addEventListener("message", async (e) => {
        const message = JSON.parse(e.data);

        if (message.join) {
          callUser();
        }

        if (message.offer) {
          handleOffer(message.offer);
        }

        if (message.answer) {
          console.log("Receiving Answer");
          peerRef.current.setRemoteDescription(
            new RTCSessionDescription(message.answer)
          );
        }

        if (message.iceCandidate) {
          console.log("Receiving and Adding ICE Candidate");
          try {
            await peerRef.current.addIceCandidate(message.iceCandidate);
          } catch (err) {
            console.log("Error Receiving ICE Candidate", err);
          }
        }
      });
    };

    startConnection();

    return () => {
      // Cleanup
      webSocketRef.current.close();
      userStream.current.getTracks().forEach((track) => track.stop());
      if (peerRef.current) {
        peerRef.current.close();
      }
    };
  }, [roomID]);

  const handleOffer = async (offer) => {
    console.log("Received Offer, Creating Answer");
    peerRef.current = createPeer();

    await peerRef.current.setRemoteDescription(
      new RTCSessionDescription(offer)
    );

    userStream.current.getTracks().forEach((track) => {
      peerRef.current.addTrack(track, userStream.current);
    });

    const answer = await peerRef.current.createAnswer();
    await peerRef.current.setLocalDescription(answer);

    webSocketRef.current.send(
      JSON.stringify({ answer: peerRef.current.localDescription })
    );
  };

  const callUser = () => {
    console.log("Calling Other User");
    peerRef.current = createPeer();

    userStream.current.getTracks().forEach((track) => {
      peerRef.current.addTrack(track, userStream.current);
    });
  };

  const createPeer = () => {
    console.log("Creating Peer Connection");
    const peer = new RTCPeerConnection({
      iceServers: [
        { urls: "stun:stun.l.google.com:19302" },
        { urls: "stun:stun.l.google.com:5349" },
        { urls: "stun:stun1.l.google.com:3478" },
        { urls: "stun:stun1.l.google.com:5349" },
        { urls: "stun:stun2.l.google.com:19302" },
        { urls: "stun:stun2.l.google.com:5349" },
        { urls: "stun:stun3.l.google.com:3478" },
        { urls: "stun:stun3.l.google.com:5349" },
        { urls: "stun:stun4.l.google.com:19302" },
        { urls: "stun:stun4.l.google.com:5349" }
    ],
    });

    peer.onnegotiationneeded = handleNegotiationNeeded;
    peer.onicecandidate = handleIceCandidateEvent;
    peer.ontrack = handleTrackEvent;

    return peer;
  };

  const handleNegotiationNeeded = async () => {
    console.log("Creating Offer");

    try {
      const myOffer = await peerRef.current.createOffer();
      await peerRef.current.setLocalDescription(myOffer);

      webSocketRef.current.send(
        JSON.stringify({ offer: peerRef.current.localDescription })
      );
    } catch (err) {
      console.error("Error during negotiation", err);
    }
  };

  const handleIceCandidateEvent = (e) => {
    console.log("Found Ice Candidate");
    if (e.candidate) {
      console.log("e.candidate: ",e.candidate);
      webSocketRef.current.send(JSON.stringify({ iceCandidate: e.candidate }));
    }
  };

  const handleTrackEvent = (e) => {
    console.log("Received Tracks");
    partnerVideo.current.srcObject = e.streams[0];
  };

  return (
    <div style={{display:'flex', width:'100%', flexDirection:'column', justifyContent:'center', alignItems:'center', gap:'20px'}}>t

      <div  style={{width:'30%'}} >
        <p>User Call</p>
        <video style={{width:'100%'}} autoPlay controls={true} ref={userVideo}></video>
      </div>

      <div style={{width:'30%'}}>
          <p>Partner Call</p>
         <video style={{width:'100%'}} autoPlay controls={true} ref={partnerVideo}></video>
      </div>
    </div>
  );
};

export default Room;
